#!/usr/bin/env ruby

# git post-receive hook for cloning the default Jenkins CI job to cover new branches as 
# they are pushed to the server.
#
# requires Ruby 1.9.3+ 

require_relative 'ci-util'
require 'rexml/document'

include REXML

# Grab the configured jenkins server
jenkins = read_config("jenkins",["url","username","password","default_job"])

# Job config template - lazy-loaded
job_template = nil

# Iterate through updated refs looking for new branches
ARGF.readlines.each { |line|
    args = line.split
    oldVal = args[0]
    newVal = args[1]
    ref = simple_branch_name args[2]

    if /^0{40}$/.match(oldVal) and ref.start_with?("refs/heads/") # new branch!        
        if httpGet(jenkins, "/job/#{ref}/config.xml", false).is_a? Net::HTTPOK
            puts "Jenkins job already exists with name: #{ref}"
            exit # abort job creation
        end

        # grab the configuration for the default job
        if (job_template == nil)            
            response = httpGet(jenkins, "/job/#{jenkins['default_job']}/config.xml")                        
            job_template = Document.new response.body
        end
        
        # overwrite the default branch with the newly pushed ref
        job_template.root.get_elements("//branches/hudson.plugins.git.BranchSpec/name").each { |elem|
            elem.text = ref
        }

        # upload the new job
        newJob = ""
        job_template.write newJob
        newJobName = ref.gsub("/", "-")
        httpPost(jenkins, "/createItem?name=#{newJobName}", newJob, 'application/xml')
    end
}
